// Alte Methode, um Module zu importieren
// const fs = require("fs")
// Um es6 imports verwenden zu können, muss in package.json das Attribut "type": "module"
import fs from "fs"

const filesFolder = "./data/"

const data = "Some Text to save into a file"

// Datei schreiben
fs.writeFile(
	`${filesFolder}some-text.txt`,
	data,
	"utf8",
	(err) => {
		if (err) {
			console.log(`Fehler: ${err}`)
			return
		}
		console.log("File wurde erfolgreich geschrieben!")
	}
)

// Datei lesen
fs.readFile(
	`${filesFolder}some-text.txt`,
	"utf8",
	(err, data) => {
		if (err) {
			console.log(`Fehler: ${err}`)
			return
		}
		console.log(data);
	}
)