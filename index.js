/*
	Erstelle einen Webserver. Es sollen die Seiten /, /shop und /imprint aufgerufen
	werden können. Alle anderen sind 404 Seiten.

	Lade bei jeder der oben angegebenen routes die entsprechende HTML Datei aus dem
	views folder und stelle diese im Browser dar (response).

	ACHTUNG: readFile/writeFile sind asynchrone Funktionen. Dh. während die Datei
	z. B. noch gelesen wird, läuft das JS weiter.
*/
import http from "http";
import fs from "fs";
import path from "path"
const views = "views";

const server = http.createServer((req, res) => {
	const url = req.url;
	let page = "";

	switch (url) {
		case "/":
			page = path.join(views, "home.html");
			break;
		case "/shop":
			page = path.join(views, "shop.html");
			break;
		case "/imprint":
			page = path.join(views, "imprint.html");
			break;
	}
	console.log(page);

	// Datei lesen
	fs.readFile(page, "utf8", (err, data) => {
		if (err) {
			res.writeHead(404, { "Content-Type": "text/html" });
			// TODO: load 404.html
			res.end(`<h1>404 not found</h1>`);
			return;
		}
		res.writeHead(200, { "Content-Type": "text/html" });
		res.end(data);
	});
});

const port = 3000;
const host = "127.0.0.1";

server.listen(port, host, () => {
	console.log(`Server erreichbar unter http://${host}:${port}`);
});
